package com.example.bankaccountserver.csharpcppjavabankaccount.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BankAccountTest {    
    private BankAccount bankAccount;

    @BeforeEach
    void setUp() {
        // Given
    	// New account is created
        UUID accountId = UUID.randomUUID();
        bankAccount = new BankAccount(accountId);
    }

    @Test
    void testCheckBalanceWithNewAccount() {
    	// Then
    	assertEquals(0.0, bankAccount.getBalance());
    }
    
    @Test
    void testDepositMoneyWithPositiveAmount() {
    	// When
    	bankAccount.depositMoney(100.0);
    	
    	// Then
        assertEquals(100.0, bankAccount.getBalance());
    }
    
    @Test
    void testDepositMoneyWithNegativeAmount() {
    	// Then
        assertThrows(IllegalArgumentException.class, () -> bankAccount.depositMoney(-100.0));
    }

    @Test
    void testWithdrawMoneyWithSufficientBalance() {
    	// Given
        bankAccount.depositMoney(200.0);
        
        // When
        bankAccount.withdrawMoney(50.0);
        
        // Then
        assertEquals(150.0, bankAccount.getBalance());
    }
    
    @Test
    void testWithdrawMoneyWithNegativeAmount() {
    	// Then
        assertThrows(IllegalArgumentException.class, () -> bankAccount.withdrawMoney(-50.0));
    }

    @Test
    void testWithdrawMoneyWithInsufficientBalance() {
    	// Then
        assertThrows(RuntimeException.class, () -> bankAccount.withdrawMoney(50.0));
    }

    @Test
    void testViewTransactionsWithDeposit() {
    	// Given
        bankAccount.depositMoney(50.0);
        
        // Then
        assertEquals(1, bankAccount.getTransactions().size());
    }

}
