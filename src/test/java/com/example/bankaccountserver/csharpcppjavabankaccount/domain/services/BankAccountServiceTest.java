package com.example.bankaccountserver.csharpcppjavabankaccount.domain.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.BankAccount;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.Transaction;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.TransactionType;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.ports.outbound.BankAccountPort;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

class BankAccountServiceTest {
	private BankAccountPort bankAccountPort;
    private BankAccountService bankAccountService;
    
    @BeforeEach
    void setUp() {
    	//Given
    	bankAccountPort = mock(BankAccountPort.class);
    	bankAccountService = new BankAccountService(bankAccountPort);
    }

    @Test
    public void testDepositMoneyValidAmount() {
    	//Given
        double initialBalance = 100.0;
        double depositAmount = 50.0;
        BankAccount bankAccount = mock(BankAccount.class);
        when(bankAccountPort.getBalance(bankAccount)).thenReturn(initialBalance);
        when(bankAccountPort.getTransactions(bankAccount)).thenReturn(new ArrayList<>());

        //When
        bankAccountService.depositMoney(bankAccount, depositAmount);

        //Then
        verify(bankAccountPort, times(1)).updateBalance(bankAccount, initialBalance + depositAmount);
        
        List<Transaction> transactions = bankAccountService.getTransactions(bankAccount);
        assertEquals(1, transactions.size());
        Transaction transaction = transactions.get(0);
        assertEquals(TransactionType.DEPOSIT, transaction.getType());
        assertEquals(depositAmount, transaction.getAmount());
    }

    @Test
    void testWithdrawMoneyValidAmount() {
        // Given
        double initialBalance = 100.0;
        double withdrawalAmount = 50.0;
        BankAccount bankAccount = mock(BankAccount.class);
        when(bankAccountPort.getBalance(bankAccount)).thenReturn(initialBalance);
        when(bankAccountPort.getTransactions(bankAccount)).thenReturn(new ArrayList<>());

        // When
        bankAccountService.withdrawMoney(bankAccount,withdrawalAmount);

        // Then
        verify(bankAccountPort, times(1)).updateBalance(bankAccount, initialBalance - withdrawalAmount);

        List<Transaction> transactions = bankAccountService.getTransactions(bankAccount);
        assertEquals(1, transactions.size());
        Transaction transaction = transactions.get(0);
        assertEquals(TransactionType.WITHDRAWAL, transaction.getType());
        assertEquals(withdrawalAmount, transaction.getAmount());
    }
    
    @Test
    public void testWithdrawMoneyInsufficientAmount() {
        //Given
    	double initialBalance = 100.0;
    	double withdrawalAmount = 150.0;
    	BankAccount bankAccount = mock(BankAccount.class);
        when(bankAccountPort.getBalance(bankAccount)).thenReturn(initialBalance);
        
        //When
        //Then
        assertThrows(IllegalArgumentException.class, () -> bankAccountService.withdrawMoney(bankAccount, withdrawalAmount));
        
        //Then
        verify(bankAccountPort, never()).updateBalance(eq(bankAccount), anyDouble());
        assertEquals(initialBalance, bankAccountService.getBalance(bankAccount));
    }
    
    @ParameterizedTest
    @ValueSource(doubles = { 0.0, -50.0 })
    public void testDepositWithdrawMoneyInvalidAmount(double withdrawalAmount) {
        //Given
    	double initialBalance = 100.0;
    	BankAccount bankAccount = mock(BankAccount.class);
        when(bankAccountPort.getBalance(bankAccount)).thenReturn(initialBalance);
        
        //When
        //Then
        assertThrows(IllegalArgumentException.class, () -> bankAccountService.depositMoney(bankAccount, withdrawalAmount));
        assertThrows(IllegalArgumentException.class, () -> bankAccountService.withdrawMoney(bankAccount, withdrawalAmount));
        
        //Then
        verify(bankAccountPort, never()).updateBalance(eq(bankAccount), anyDouble());
        assertEquals(initialBalance, bankAccountService.getBalance(bankAccount));
    }
    
    @Test
    void testGetBalance() {
		//Given
        double initialBalance = 100.0;
        BankAccount bankAccount = mock(BankAccount.class);
        when(bankAccountPort.getBalance(bankAccount)).thenReturn(initialBalance);

        //When
        double retrievedBalance = bankAccountService.getBalance(bankAccount);

        //Then
        assertEquals(initialBalance, retrievedBalance);
        verify(bankAccountPort, times(1)).getBalance(bankAccount);
    }

    @Test
    void testGetTransactions() {
    	//Given
    	List<Transaction> transactions = new ArrayList<>();
    	BankAccount bankAccount = mock(BankAccount.class);
    	transactions.add(new Transaction(bankAccount, TransactionType.DEPOSIT, 50.0));
    	transactions.add(new Transaction(bankAccount, TransactionType.WITHDRAWAL, 20.0));
        when(bankAccountPort.getTransactions(bankAccount)).thenReturn(transactions);

        //When
        List<Transaction> retrievedTransactions = bankAccountService.getTransactions(bankAccount);

        //Then
        assertEquals(transactions.size(), retrievedTransactions.size());
        assertTrue(retrievedTransactions.containsAll(transactions));
        verify(bankAccountPort, times(1)).getTransactions(bankAccount);
    }
}