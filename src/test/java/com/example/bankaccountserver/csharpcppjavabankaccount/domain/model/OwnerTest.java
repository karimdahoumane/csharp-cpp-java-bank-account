package com.example.bankaccountserver.csharpcppjavabankaccount.domain.model;

import org.junit.jupiter.api.Test;

import java.util.UUID;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OwnerTest {

	@Test
	public void testOwnerInitialization() {
        // Given
        UUID ownerId = UUID.randomUUID();
        String ownerName = "Karim Dahoumane";

        // When
        Owner owner = new Owner(ownerId, ownerName);

        // Then
        assertEquals(ownerId, owner.getId());
        assertEquals(ownerName, owner.getName());
    }
}