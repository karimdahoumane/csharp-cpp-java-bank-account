package com.example.bankaccountserver.csharpcppjavabankaccount.domain.model;

import org.junit.jupiter.api.Test;

import java.util.UUID;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankAccountTest {
    
    @Test
    void testBankAccountInitialization() {
        // Given
        UUID accountId = UUID.randomUUID();
        UUID ownerId = UUID.randomUUID();
        String ownerName = "Karim Dahoumane";
        Owner owner = new Owner(ownerId, ownerName);

        // When
        BankAccount bankAccount = new BankAccount(accountId, owner);

        // Then
        assertEquals(accountId, bankAccount.getId());
        assertEquals(owner, bankAccount.getOwner());
    }
}