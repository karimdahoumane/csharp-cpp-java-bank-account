package com.example.bankaccountserver.csharpcppjavabankaccount.domain.model;

import org.junit.jupiter.api.Test;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TransactionTest {
    
    @Test
    void testTransactionInitialization() {
        // Given
        UUID transactionId = UUID.randomUUID();
        UUID accountId = UUID.randomUUID();
        Owner owner = new Owner(UUID.randomUUID(), "John Doe");
        BankAccount bankAccount = new BankAccount(accountId, owner);
        TransactionType transactionType = TransactionType.DEPOSIT;
        double transactionAmount = 100.0;

        // When
        Transaction transaction = new Transaction(transactionId, bankAccount, transactionType, transactionAmount);

        // Then
        assertNotNull(transaction.getId());
        assertEquals(transactionId, transaction.getId());
        assertEquals(bankAccount, transaction.getBankaccount());
        assertEquals(transactionType, transaction.getType());
        assertEquals(transactionAmount, transaction.getAmount());
        assertNotNull(transaction.getDate());
    }
}