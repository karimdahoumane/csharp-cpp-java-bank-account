package com.example.bankaccountserver.csharpcppjavabankaccount.domain.model;

import java.time.LocalDateTime;
import java.util.UUID;

import lombok.Getter;

@Getter
public class Transaction {
	private UUID id;
	private BankAccount bankaccount;
	private TransactionType type;
	private double amount;
	private LocalDateTime date;

    public Transaction(UUID id, BankAccount bankAccount, TransactionType type, double amount) {
    	this.id = id;
    	this.bankaccount = bankAccount;
        this.type = type;
        this.amount = amount;
        this.date = LocalDateTime.now();
    }
    
    public Transaction(BankAccount bankAccount, TransactionType type, double amount) {
    	this.bankaccount = bankAccount;
        this.type = type;
        this.amount = amount;
        this.date = LocalDateTime.now();
    }
}