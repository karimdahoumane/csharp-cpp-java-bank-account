package com.example.bankaccountserver.csharpcppjavabankaccount.domain.ports.inbound;

import java.util.List;

import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.BankAccount;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.Transaction;

public interface BankAccountUseCase {
    void depositMoney(BankAccount bankAccount, double amount);
    void withdrawMoney(BankAccount bankAccount, double amount);
    double getBalance(BankAccount bankAccount);
    List<Transaction> getTransactions(BankAccount bankAccount);
}
