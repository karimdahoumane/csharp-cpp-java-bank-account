package com.example.bankaccountserver.csharpcppjavabankaccount.domain.model;

import java.util.UUID;

import lombok.Getter;

@Getter
public class Owner {
    private UUID id;
    private String name;

    public Owner(UUID id, String name) {
        this.id = id;
        this.name = name;
    }
}