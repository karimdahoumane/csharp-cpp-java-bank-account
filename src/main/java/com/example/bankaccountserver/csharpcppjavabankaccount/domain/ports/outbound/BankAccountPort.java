package com.example.bankaccountserver.csharpcppjavabankaccount.domain.ports.outbound;

import java.util.List;

import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.BankAccount;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.Transaction;

public interface BankAccountPort {
	double getBalance(BankAccount bankAccount);
    void updateBalance(BankAccount bankAccount, double newBalance);
    List<Transaction> getTransactions(BankAccount bankAccount);
}