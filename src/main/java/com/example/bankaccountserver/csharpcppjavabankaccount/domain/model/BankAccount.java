package com.example.bankaccountserver.csharpcppjavabankaccount.domain.model;

import java.util.UUID;

import lombok.Getter;

@Getter
public class BankAccount {
	private final UUID id;
	private final Owner owner;

    public BankAccount(UUID id, Owner owner) {
    	this.id = id;
    	this.owner = owner;
    }
}