package com.example.bankaccountserver.csharpcppjavabankaccount.domain.services;

import java.util.ArrayList;
import java.util.List;

import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.BankAccount;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.Transaction;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.model.TransactionType;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.ports.inbound.BankAccountUseCase;
import com.example.bankaccountserver.csharpcppjavabankaccount.domain.ports.outbound.BankAccountPort;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BankAccountService implements BankAccountUseCase {
    private final BankAccountPort bankAccountPort;

    @Override
    public void depositMoney(BankAccount bankAccount, double amount) {
        validateAmount(amount);

        double currentBalance = bankAccountPort.getBalance(bankAccount);
        double newBalance = currentBalance + amount;
        bankAccountPort.updateBalance(bankAccount, newBalance);
        Transaction transaction = new Transaction(bankAccount, TransactionType.DEPOSIT, amount);
        bankAccountPort.getTransactions(bankAccount).add(transaction);
    }

    @Override
    public void withdrawMoney(BankAccount bankAccount, double amount) {
        validateAmount(amount);

        double currentBalance = bankAccountPort.getBalance(bankAccount);
        if (amount > currentBalance) {
            throw new IllegalArgumentException("Insufficient funds for withdrawal");
        }

        double newBalance = currentBalance - amount;
        bankAccountPort.updateBalance(bankAccount, newBalance);
        Transaction transaction = new Transaction(bankAccount, TransactionType.WITHDRAWAL, amount);
        bankAccountPort.getTransactions(bankAccount).add(transaction);
    }

    @Override
    public double getBalance(BankAccount bankAccount) {
        return bankAccountPort.getBalance(bankAccount);
    }

    @Override
    public List<Transaction> getTransactions(BankAccount bankAccount) {
        return new ArrayList<>(bankAccountPort.getTransactions(bankAccount));
    }
    
    private void validateAmount(double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount must be greater than zero");
        }
    }
}