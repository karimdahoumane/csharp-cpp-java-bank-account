package com.example.bankaccountserver.csharpcppjavabankaccount.domain.model;

public enum TransactionType {
	WITHDRAWAL,
    DEPOSIT
}